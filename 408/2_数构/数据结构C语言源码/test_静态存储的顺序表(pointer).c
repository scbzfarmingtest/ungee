//静态存储的顺序表源码（用链表指针定义链表）
//有点坑的是平板居然运行不成，在线编译没问题。
/*
总结：
1. typedef要写main函数外面；
2. 如果不想main函数里面声明，函数要写在main函数前面；
3. 不要使用未定义的指针。如不能Status GetElem(SqList *L, int i, Elem *e){...；return *e};
4. 即使用指针定义线性表（推广至其他结构体），也要定义线性表，防止出现错误3；
5. 删除后len变没变？/插入超没超最大值？/插入的函数插没插进去？/插入后len变没变？
*/

# include <stdio.h>
# define OK 1
# define TRUE 1
# define FALSE 0
# define ERROR -1
# define max 20                 //假定链表最长含20个数据元素

typedef int Status;
typedef int Elem;
typedef struct SqList{
    Elem n[max];
    int len;
}SqList;


Status InitList(SqList *L){
    L->len=0;
    return OK;
}

Status ListEmpty(SqList *L){
    if (L->len==0) return TRUE;
    else if (L->len>0)  return FALSE;
    else return ERROR;
}

Status ListLength(SqList *L){
    return L->len;
}

Status ClearList(SqList *L){
    L->len=0;
    return OK;
}

Status GetElem(SqList *L, int i){
    if(L->len==0)      return ERROR;
    if(i<=0||i>L->len) return ERROR;
    return(L->n[i-1]);
}

Status LocateElem(SqList *L,Elem e){
    if(L->len==0)  return ERROR;
    for(int k=1;k<=L->len;k++){
        if (L->n[k-1]==e)
        return L->n[k-1];
    }
    return ERROR;
}

Status ListInsert(SqList *L,int i,Elem e){
    if(L->len==0) return ERROR;
    if(i<=0||i>L->len) return ERROR;
    if(i+1>max) return ERROR;
    for(int k=L->len;k>=i;--k){
        L->n[k]=L->n[k-1];
    }
	L->n[i-1]=e;
    ++L->len;
    return OK;
}

Status ListDelete(SqList *L,int i,Elem e){
    if(L->len==0) return ERROR;
    if(i<1||i>L->len) return ERROR;
    e=L->n[i-1];
	for(int k=i;k<=L->len-1;++k){
        L->n[k-1]=L->n[k];
    }
    --L->len;
    return e;
}

Status CreateList(SqList *L,Elem e[],int n){
    if(n>max) return ERROR;
    L->len=n;
    for(int k=1;k<=n;++k){
        L->n[k-1]=e[k-1];
    }
    return OK;
}

int main(){
    int a[max]={1,2,3,4,5};
    int o[2][10];
    int i, a_len;
    Elem em=100;
	Elem *j;
	j=NULL;
    SqList *L1, *L2, L01, L02;
	L1=&L01;
	L2=&L02;
    o[0][0]=CreateList(L1,a,a_len=5);
	o[1][0]=InitList(L2);
    o[0][1]=ListEmpty(L1);
    o[1][1]=ListEmpty(L2);
    o[0][2]=ListLength(L1);
    o[1][2]=ListLength(L2);
    o[0][3]=ListInsert(L1,i=1,em=100);
    o[1][3]=ListInsert(L2,i=1,em=100);
    if(o[1][3]==-1){
        printf("1OK\n");
        o[1][3]=ListInsert(L2,i=0,em=100);
    }
    o[0][4]=LocateElem(L1,em=100-1);
    o[0][4]=LocateElem(L2,em=100);
    o[0][5]=ListDelete(L1,i=4,em=100);
    o[1][5]=ListDelete(L2,i=0,em=100);
    o[0][6]=GetElem(L1,i=3,em=100);
    o[1][6]=GetElem(L1,i=0,em=100);
    o[0][7]=ClearList(L1);
    o[1][7]=ClearList(L2);
    for(int p=0;p<=7;++p){
        printf("%d\t%d\n",o[0][p],o[1][p]);
    }
    getchar();
    return 0;
}
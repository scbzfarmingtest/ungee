//静态存储的顺序表源码（用链表指针定义链表）

# include <stdio.h>
# define OK 1
# define TRUE 1
# define FALSE 0
# define ERROR -1
# define max 20                 //假定链表最长含20个数据元素

typedef int Status;
typedef int Elem;
typedef struct SqList{
    Elem n[max];
    int len;
}SqList;


Status InitList(SqList *L){
    L->len=0;
    return OK;
}

Status ListEmpty(SqList *L){
    if (L->len==0) return TRUE;
    else if (L->len>0)  return FALSE;
    else return ERROR;
}

Status ListLength(SqList *L){
    return L->len;
}

Status ClearList(SqList *L){
    L->len=0;
    return OK;
}

Status GetElem(SqList *L, int i){
    if(L->len==0)      return ERROR;
    if(i<=0||i>L->len) return ERROR;
    return(L->n[i-1]);
}

Status LocateElem(SqList *L,Elem e){
    if(L->len==0)  return ERROR;
    for(int k=1;k<=L->len;k++){
        if (L->n[k-1]==e)
        return L->n[k-1];
    }
    return ERROR;
}

Status ListInsert(SqList *L,int i,Elem e){
    if(L->len==0) return ERROR;
    if(i<=0||i>L->len) return ERROR;
    if(i+1>max) return ERROR;
    for(int k=L->len;k>=i;--k){
        L->n[k]=L->n[k-1];
    }
	L->n[i-1]=e;
    ++L->len;
    return OK;
}

Status ListDelete(SqList *L,int i,Elem e){
    if(L->len==0) return ERROR;
    if(i<1||i>L->len) return ERROR;
    e=L->n[i-1];
	for(int k=i;k<=L->len-1;++k){
        L->n[k-1]=L->n[k];
    }
    --L->len;
    return e;
}

Status CreateList(SqList *L,Elem e[],int n){
    if(n>max) return ERROR;
    L->len=n;
    for(int k=1;k<=n;++k){
        L->n[k-1]=e[k-1];
    }
    return OK;
}

int main(){
    return 0;
}
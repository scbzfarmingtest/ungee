# include <stdio.h>
# define OK 1
# define True 1
# define False 0
# define ERROR -1
# define max 20

typedef int Elem;
typedef int Status;
typedef struct Sqlist{
    Elem n[max];
    int len;
}Sqlist;

Status InitList(Sqlist L){
    L.len=0;
    return OK;
}

Status CreateList(Sqlist L,Elem a[],int a_len){
    if(a_len>=max) return ERROR;
    L.len=a_len;
    for(int k=1;k<=L.len;++k){
        L.n[k-1]=a[k-1];
    }
    return OK;
}

Status ListEmpty(Sqlist L){
    if(L.len==0) return True;
    else if(L.len>0) return False;
    else return ERROR;
}

Status ListLength(Sqlist L){
    return L.len;
}

Status InsertList(Sqlist L,int i,Elem e){
    if(L.len==0) return ERROR;
    else if(i<1||i>L.len) return ERROR;
    else if(i+1>=max) return ERROR;
    for(int k=L.len;k>=i;++k){
        L.n[k]=L.n[k-1];
    }
    L.n[i-1]=e;
    ++L.len;
    return OK;
}

Status DeleteList(Sqlist L,int i,Elem e){
    if(L.len==0) return 0;
    else if(i<1||i>L.len) return ERROR;
    e=L.n[i-1];
    for(int k=i;k<=L.len;++k){
        L.n[k-1]=L.n[k];
    }
    --L.len;
    return OK;
}

Status LocateElem(Sqlist L,Elem e){
    if(L.len==0) return ERROR;
    for(int k=1;k<=L.len;++k){
        if(L.n[k-1]==e) return k;
    }
    return ERROR;
}

Status GetElem(Sqlist L,int i,Elem e){
    if(L.len==0) return ERROR;
    if(i<1||i>L.len) return ERROR;
    return L.n[i-1];
}

Status ClearList(Sqlist L){
    L.len=0;
    return OK;
}

int main(){
    return 0;
}